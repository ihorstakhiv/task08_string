package com.epam.controllers;

import com.epam.helper.ConstNumbers;
import com.epam.helper.ConstText;
import com.epam.models.Sentence;
import com.epam.service.FilesReader;
import java.util.*;
import java.util.stream.Collectors;

public class Controller {

    public List<Map.Entry<Integer, Sentence>> findSentenceWhereTheWordsReplays() {
        List<Sentence> sentences = new FilesReader().splitIntoSentence();
        HashMap<Integer, Sentence> singleWords = new HashMap<>();
        for (Sentence sentence : sentences) {
            for (String word : sentence.words) {
                int wordCount = ConstNumbers.getGetFirstElement();
                for (String wordNext : sentence.words) {
                    if (word.equals(wordNext)) {
                        wordCount++;
                        if (wordCount > ConstNumbers.getElementRepeat()) {
                            singleWords.put(wordCount, sentence);
                        }
                    }
                }
            }
        }
        return singleWords.entrySet().stream()
                                     .distinct()
                                     .sorted((o1, o2) -> o2.getKey() - o1.getKey())
                                     .collect(Collectors.toList());
    }

    public List<Sentence> getAllSentencesAscendingOrder() {
        return new FilesReader().splitIntoSentence().stream()
                                                    .sorted((o1, o2) -> o1.value.length() - o2.value.length())
                                                    .collect(Collectors.toList());
    }

    public void findWordFirstSentenceWhichAllSentenceDoesNotHave() {
        Set<String> finalList = new HashSet<>();
        List<Sentence> sentences = new FilesReader().splitIntoSentence();
        List<String> wordsFirst = sentences.get(ConstNumbers.getGetFirstElement()).getWords();
        Iterator<String> iterator = wordsFirst.iterator();
        while (iterator.hasNext()) {
            for (Sentence sentence : sentences) {
                for (String word : sentence.getWords()) {
                    if (iterator.next().equals(word)) {
                        finalList.add(iterator.next());
                    }
                }
            }
        }
    }

    public List getAllWordsFixedLengthInQuestions(int size) {
        List<String> wordsFinal = new LinkedList<>();
        List<Sentence> questions = new LinkedList<>();
        List<Sentence> sentences = new FilesReader().splitIntoSentence();
        sentences.stream()
                 .filter(s -> s.getValue()
                 .contains("?"))
                 .forEach(questions::add);
        for (Sentence s : questions) {
            List<String> collect = Arrays.stream(s.getValue()
                                         .split(ConstText.getSplitSpace()))
                                         .collect(Collectors.toList());
            for (String word : collect) {
                if (word.length() == size) {
                    wordsFinal.add(word);
                }
            }
        }
        return wordsFinal;
    }

    public Set<String> changeFirstLoudWordOnLongest() {
        Set<String> wordFixedLength = new LinkedHashSet<>();
        List<Sentence> sentences = new FilesReader().splitIntoSentence();
        for (Sentence sentence : sentences) {
            for (String word : sentence.getWords()) {
                char firstLoudLetter = word.charAt(ConstNumbers.getGetFirstElement());
            }
        }
        return wordFixedLength;
    }

    public List<String> sortByVowel() {
        String fullText = new FilesReader().text;
        String[] allWords = fullText.split(ConstText.getSplitSpace());
        return Arrays.stream(allWords)
                     .sorted(String::compareTo)
                     .collect(Collectors.toList());
    }

    public List<Map.Entry<Integer, String>> sortByPercentLoudLetters() {
        Map<Integer, String> wordPercent = new HashMap<>();
        String fullText = new FilesReader().text;
        String[] words = fullText.split(ConstText.getSplitSpace());
        for (String word : words) {
            int countOf = ConstNumbers.getGetFirstElement();
            String[] split = word.split(ConstText.getSplitLetter());
            for (String letter : split) {
                if (letter.matches(ConstText.getRegexVowel())) {
                    countOf++;
                }
            }
            int formula = countOf * ConstNumbers.getMaxPercent() / word.length();
            wordPercent.put(formula, word);
        }
        return wordPercent.entrySet().stream()
                                     .sorted((o1, o2) -> o1.getKey() - o2.getKey())
                                     .collect(Collectors.toList());
    }

    public void deleteSubstringWhichStartWithInputLetterAndFinish(String startLetter, String endLetter) {
        String text = new FilesReader().text;
    }

    public String[] deleteAllWordsWhichStartWithConsonant() {
        String read = new FilesReader().text;
        String[] words = read.split(" ");
        for (String word : words) {
            String[] split = word.split("");
            if (split[ConstNumbers.getGetFirstElement()].matches(ConstText.getRegexConsonant())) {
                for (int i = ConstNumbers.getStartCycle(); i < words.length; i++) {
                    if (words[i].equals(word)) {
                        words[i] = "";
                    }
                }
            }
        }
        return words;
    }

    public List sortWordsDecrease() {
        String read = new FilesReader().text;
        List<String> allWords = Arrays.stream(read.split(" "))
                                      .collect(Collectors.toList());
        List<String> countAndWords = new LinkedList<>();
        for (String word : allWords) {
            for (String wordSecond : allWords) {
                int wordCount = ConstNumbers.getStartCycle();
                if (word.equals(wordSecond)) {
                    wordCount++;
                    if (wordCount >= ConstNumbers.getElementRepeat()) {
                        countAndWords.add(word);
                    }
                }
            }
        }
        return countAndWords;
    }

    public String findPolydrom() {
        String read = new FilesReader().text;
        String[] allWords = read.split(" ");
        List<String> allText = new LinkedList<>();
        for (String s : allWords) {
            StringBuilder reverse = new StringBuilder(s);
            if (s.equals(reverse.reverse().toString())) {
                allText.add(s);
            }
        }
        return allText.stream()
                      .sorted((o1, o2) -> o2.length() - o1.length())
                      .collect(Collectors.toList())
                      .get(ConstNumbers.getGetFirstElement());
    }

    public List<String> deleteFirstLetterEachWord() {
        String read = new FilesReader().text;
        String[] allWords = read.split(" ");
        List<String> sentencesWhithOutFirstLetter = new LinkedList<>();
        for (String s : allWords) {
            sentencesWhithOutFirstLetter.add(s.substring(1));
        }
        return sentencesWhithOutFirstLetter;
    }

    public List<Sentence> changeFixedWordAnotherWord(int sentenceNumber, int wordLength, String newWord) {
        List<Sentence> allSentences = new FilesReader().splitIntoSentence();
        for (String word : allSentences.get(sentenceNumber).getWords()) {
            if (word.length() == wordLength) {
                Collections.replaceAll(allSentences.get(sentenceNumber).getWords(), word, newWord);
            }
        }
        return allSentences;
    }
}






