package com.epam.helper;

public class ConstText {

    static final String INPUT_CHOOSE ="Please, write your choice :";
    static final String FAIL_CHOOSE ="Your choice is invalid, try again :";
    static final String FILE_PATH ="D://JAVA//task08_String//src//main//resources//a.txt";
    static final String REGEX_VOWEL ="[аеуоіїяєи]";
    static final String REGEX_CONSONANT ="[бвгджзйклмнпрстфхцчшщБВГДЖЄЙКЛМНПРСТФХЦЧШЩ]";
    static final String SPLIT_SPACE =" ";
    static final String SPLIT_LETTER ="";

    public static String getInputChoose() {
        return INPUT_CHOOSE;
    }

    public static String getFailChoose() {
        return FAIL_CHOOSE;
    }

    public static String getFilePath() {
        return FILE_PATH;
    }

    public static String getRegexVowel() {
        return REGEX_VOWEL;
    }

    public static String getRegexConsonant() {
        return REGEX_CONSONANT;
    }

    public static String getSplitSpace() {
        return SPLIT_SPACE;
    }

    public static String getSplitLetter() {
        return SPLIT_LETTER;
    }
}
