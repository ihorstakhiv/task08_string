package com.epam.helper;

public class ConstNumbers {

    static int START_CYCLE = 0;
    static int GET_FIRST_ELEMENT = 0;
    static int ELEMENT_REPEAT = 1;
    static int MAX_PERCENT = 100;

    public static int getMaxPercent() {
        return MAX_PERCENT;
    }

    public static int getElementRepeat() {
        return ELEMENT_REPEAT;
    }

    public static int getStartCycle() {
        return START_CYCLE;
    }

    public static int getGetFirstElement() {
        return GET_FIRST_ELEMENT;
    }
}
