package com.epam.models;

import java.util.List;

public class Text {

    public  List<Sentence> allSentence;

    public Text() {
    }

    public Text(List<Sentence> allSentence) {
        this.allSentence = allSentence;
    }

    public List<Sentence> getAllSentence() {
        return allSentence;
    }

    public void setAllSentence(List<Sentence> allSentence) {
        this.allSentence = allSentence;
    }

    @Override
    public String toString() {
        return "Text{" +
                "allSentence=" + allSentence +
                '}';
    }
}
