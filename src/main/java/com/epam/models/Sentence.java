package com.epam.models;

import java.util.List;

public class Sentence {

   public String value;
   public List<String> words;

    public Sentence(String value, List<String> words) {
        this.value = value;
        this.words = words;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<String> getWords() {
        return words;
    }

    public void setWords(List<String> words) {
        this.words = words;
    }

    @Override
    public String toString() {
        return "Sentence{" +
                "value='" + value + '\'' +
                ", words=" + words +
                '}';
    }
}

