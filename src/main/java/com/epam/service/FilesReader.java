package com.epam.service;

import com.epam.helper.ConstText;
import com.epam.models.Sentence;
import com.epam.models.Text;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FilesReader implements Reader {

    public String text;

    public FilesReader() {
        text = read(ConstText.getFilePath());
    }

    @Override
    public String read(String file) {
        String text = "";
        StringBuilder allText = new StringBuilder(text);
        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File(file)));
            while ((text = reader.readLine()) != null) {
                allText.append(text);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        } catch (IOException e) {
            System.out.println("I/O error.");
        }
        return allText.toString();
    }

    public List<Sentence> splitIntoSentence() {
        Text text = new Text();
        List<Sentence> sentencesFinal = new ArrayList<>();
        List<String> sentences = Arrays.stream(this.text
                                        .split("(?<=[.?!])"))
                                        .collect(Collectors.toList());
        for (String s : sentences) {
            List<String> words = Arrays.stream(s.split(" "))
                                       .collect(Collectors.toList());
            sentencesFinal.add(new Sentence(s, words));
        }
        text.setAllSentence(sentencesFinal);
        return text.getAllSentence();
    }
}
