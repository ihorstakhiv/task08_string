package com.epam.service;

public interface Reader {

     String read(String file);
}
