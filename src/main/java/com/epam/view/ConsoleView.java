package com.epam.view;

import com.epam.controllers.Controller;
import com.epam.helper.ConstText;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ConsoleView implements View {

    private Locale locale;
    private ResourceBundle resourceBundle;
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);

    public ConsoleView() {
        input = new Scanner(System.in);
        controller = new Controller();
        logger.info("Please chose a language \n" + "\n1) English" + "\n2) Українська \n");
        try {
            int choose = input.nextInt();
            if (choose == 1)
                englishMenu();
            if (choose == 2)
                ukraineMenu();
            if (choose > 2)
                throw new NullPointerException();
        } catch (Exception exception) {
            logger.info("\nInvalid variable, please enter from 1 to 2\n");
            new ConsoleView();
        }
    }

    private void putResourceBundle() {
        menu = new LinkedHashMap<>();
        menu.put("1", resourceBundle.getString("1"));
        menu.put("2", resourceBundle.getString("2"));
        menu.put("3", resourceBundle.getString("3"));
        menu.put("4", resourceBundle.getString("4"));
        menu.put("5", resourceBundle.getString("5"));
        menu.put("6", resourceBundle.getString("6"));
        menu.put("7", resourceBundle.getString("7"));
        menu.put("8", resourceBundle.getString("8"));
        menu.put("9", resourceBundle.getString("9"));
        menu.put("10", resourceBundle.getString("10"));
        menu.put("11", resourceBundle.getString("11"));
        menu.put("12", resourceBundle.getString("12"));
        menu.put("13", resourceBundle.getString("13"));
        menu.put("14", resourceBundle.getString("14"));
        menu.put("15", resourceBundle.getString("15"));
        menu.put("16", resourceBundle.getString("16"));
        menu.put("17", resourceBundle.getString("17"));
    }

    private void putMethods() {
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::printFindSentenceWhereTheWordsReplays);
        methodsMenu.put("2", this::printAllSentencesAscendingOrder);
        methodsMenu.put("3", this::printFindWordFirstSentenceWhichAllSentenceDoesNotHave);
        methodsMenu.put("4", this::printAllWordsFixedLengthInQuestions);
        methodsMenu.put("5", this::printChangeFirstLoudWordOnLongest);
        methodsMenu.put("6", this::printSortByVowel);
        methodsMenu.put("7", this::printSortByPercentLoudLetters);
        methodsMenu.put("8", this::theWordsStartingWithVowelsAreSortedAlphabeticallyByTheFirstConsonant);
        methodsMenu.put("9", this::wordsWithTheSameNumberPlacedInAlphabeticalOrder);
        methodsMenu.put("10", this::findHowManyTimesItOccursInEachSentence);
        methodsMenu.put("11", this::removeTheSubstratumOfTheMaximumLength);
        methodsMenu.put("12", this::printDeleteAllWordsWhichStartWithConsonant);
        methodsMenu.put("13", this::printSortWordsDecrease);
        methodsMenu.put("14", this::printFindPolydrom);
        methodsMenu.put("15", this::printDeleteFirstLetterEachWord);
        methodsMenu.put("16", this::printChangeFixedWordAnotherWord);
        methodsMenu.put("17", this::languageMenu);
    }

    private void removeTheSubstratumOfTheMaximumLength() {
        controller.deleteFirstLetterEachWord();
    }

    private void findHowManyTimesItOccursInEachSentence() {
        logger.info(controller.findSentenceWhereTheWordsReplays());
    }

    private void wordsWithTheSameNumberPlacedInAlphabeticalOrder() {
        logger.info(controller.sortByPercentLoudLetters());
    }

    private void theWordsStartingWithVowelsAreSortedAlphabeticallyByTheFirstConsonant() {
        logger.info(controller.sortByVowel());
    }

    private void printFindSentenceWhereTheWordsReplays() {
        logger.info(controller.findSentenceWhereTheWordsReplays());
    }

    private void printAllSentencesAscendingOrder() {
        logger.info(controller.getAllSentencesAscendingOrder());
    }

    private void printFindWordFirstSentenceWhichAllSentenceDoesNotHave() {
        controller.findWordFirstSentenceWhichAllSentenceDoesNotHave();
    }

    private void printAllWordsFixedLengthInQuestions() {
        logger.info(ConstText.getInputChoose() + " word length: ");
        try {
            logger.info(controller.getAllWordsFixedLengthInQuestions(input.nextInt()));
        } catch (Exception exception) {
            logger.info(ConstText.getFailChoose());
            logger.fatal(exception.getStackTrace());
            printAllWordsFixedLengthInQuestions();
        }
    }

    private void printChangeFirstLoudWordOnLongest() {
        logger.info(controller.changeFirstLoudWordOnLongest());
    }

    private void printSortByVowel() {
        logger.info(controller.sortByVowel());
    }

    private void printSortByPercentLoudLetters() {
        logger.info(controller.sortByPercentLoudLetters());
    }

    private void printDeleteAllWordsWhichStartWithConsonant() {
        logger.info(controller.deleteAllWordsWhichStartWithConsonant());
    }

    private void printSortWordsDecrease() {
        logger.info(controller.sortWordsDecrease());
    }

    private void printFindPolydrom() {
        logger.info(controller.findPolydrom());
    }

    private void printDeleteFirstLetterEachWord() {
        logger.info(controller.deleteFirstLetterEachWord());
    }

    private void printChangeFixedWordAnotherWord() {
        logger.info(ConstText.getInputChoose());
        logger.info("You have to insert 3 variables (sentence number, word length, new word):");
        try {
            logger.info(controller.changeFixedWordAnotherWord(input.nextInt(), input.nextInt(), input.nextLine()));
        } catch (Exception exception) {
            logger.info(ConstText.getFailChoose());
            logger.fatal(exception.getStackTrace());
            printChangeFixedWordAnotherWord();
        }
    }

    private void englishMenu() {
        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle("MyMenu", locale);
        putResourceBundle();
        putMethods();
        show();
    }

    private void ukraineMenu() {
        locale = new Locale("uk");
        resourceBundle = ResourceBundle.getBundle("MyMenu", locale);
        putResourceBundle();
        putMethods();
        show();
    }

    private void languageMenu() {
        new ConsoleView();
    }

    private void process() {
        logger.info("in process");
    }

    private void outputMenu() {
        logger.info("\n*********************************************       MENU       *********************************************   \n");
        for (String str : menu.values()) {
            logger.info("\n" + str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("\nPlease, select menu point:\n");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.getStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}